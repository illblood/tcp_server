package main

import (
	"zlink.ltd/tcp_server/src/config"
	_ "zlink.ltd/tcp_server/src/config"
	tcp_server "zlink.ltd/tcp_server/src/server"
)

func main() {
	port := config.Config.App.Port

	tcp_server.RunServer(port)
}
