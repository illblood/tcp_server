set version=2.2
echo %version%

SET GOOS=windows
SET GOARCH=amd64
go build main.go
ren main.exe tcp_server_win_%version%.exe

SET CGO_ENABLED=0
SET GOOS=linux
SET GOARCH=amd64
go build  -a -installsuffix cgo  main.go


ren main tcp_server_linux_%version%